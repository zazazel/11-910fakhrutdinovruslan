﻿using System;

namespace Lab7
{
    class Program
    {
        static void Randoms(int[,] a)
        {
            Random rand = new Random();

            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                    a[i, j] = rand.Next(1, 10);
        }
        static void Randoms2(int[,] a)
        {
            Random rand = new Random();

            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                    a[i, j] = rand.Next(0, 2);
        }




        static void random_Kvadrat(int[,] a)
        {
            Random rand = new Random();
            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                    a[i, j] = rand.Next(1, 10);
        }


        static void Sum_elem(int[,] a)
        {
            int sum = 0;
            for (int i = 0; i < a.GetLength(0); i++)
                sum = sum + a[i, i];
            Vivod_otvet("Сумма эллементов диагонали = ");
            Vivod_otvet(Convert.ToString(sum));
        }




        static void POB_DIAG(int[,] a)
        {
            int k = 0;
            for (int i = 0; i < a.GetLength(0); i++)
                if (a[i, a.GetLength(0) - 1 - i] % 2 == 0)
                    k++;
            Vivod_otvet("В побочной диагонале  ");
            Vivod_otvet(Convert.ToString(k));
            Vivod_otvet("  четных эллементов");
        }


        static int[] ONEMASS(int[,] a)
        {
            int[] z = new int[a.GetLength(1)];
            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                    z[j] = z[j] + a[i, j];
            return z;
        }


        static void onemas_vivod(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
                Console.Write(a[i] + " ");
        }
        static int[] ONEMASS_str(int[,] a)
        {
            int k;
            int[] z = new int[a.GetLength(0)];
            for (int i = 0; i < a.GetLength(0); i++)
            {
                k = 0;
                for (int j = 0; j < a.GetLength(1); j++)
                    if (k < a[i, j]) k = a[i, j];
                z[i] = k;
            }
            return z;
        }

        static void mas7(int[,] M7)
        {
            for (int i = 0; i < M7.GetLength(0); i++)
            {
                M7[i, i] = 1;
                M7[i, M7.GetLength(0) - 1 - i] = 1;
            }

        }

        static void M5(int[,] m5)
        {
            int z = 1;
            int i, j;
            i = 0;
            j = i;
            while (z < 26)
            {
                while ((j < m5.GetLength(0)) && (m5[i, j] == 0))
                {
                    m5[i, j] = z;

                    z++;
                    j++;
                }
                j--;
                i++;
                while ((i < m5.GetLength(0)) && (m5[i, j] == 0))
                {
                    m5[i, j] = z;
                    i++;
                    z++;
                }
                i--;
                j--;
                while (j > -1 && m5[i, j] == 0)
                {
                    m5[i, j] = z;
                    z++;
                    j--;
                }
                j++;
                i--;
                while (m5[i, j] == 0)
                {
                    m5[i, j] = z;
                    z++;
                    i--;
                }
                i++;
                j++;
            }


        }
        static void NULL(int[,] a)
        {
            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                    a[i, j] = 0;
        }





        static void vivod3(int[,] a)
        {
            int z = 0;
            for (int j = 0; j < a.GetLength(1); j++)
                z = z + a[2, j];
            Vivod_otvet(Convert.ToString(z));
        }
        static void vivod10(int[,] a)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    Console.Write(a[i, j] + " ");
                    if (a[i, j] < 10) Console.Write(" ");
                }
                Console.WriteLine();

            }
                Console.ForegroundColor = ConsoleColor.DarkCyan;
            }


        static void vivod(int[,] a)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                    Console.Write(a[i, j] + " ");
                Console.WriteLine();
            }

                Console.ForegroundColor = ConsoleColor.White;
        }
        static void Mesta(int[,] a)
        {
            int sum = 0;
            for (int i = 0; i < a.GetLength(0); i++)
                for (int j = 0; j < a.GetLength(1); j++)
                    sum = sum + a[i, j];
            Vivod_otvet("В поезде ");
            Vivod_otvet(Convert.ToString(648 - sum));
            Vivod_otvet(" свободных мест");

        }
        static void Vivod_otvet(string a)
        {
            Console.Write(a + "  ");
        }


        static void vivodStroki(int[,] a)
        {
            int z = -1;
            while (z < 0 || z > a.GetLength(0))
            {
                Console.WriteLine("Введите номер строки от" + " 1" + " до " + (a.GetLength(0)));
                z = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine();
            for (int i = 0; i < a.GetLength(1) - 1; i++) Console.Write(a[z - 1, i] + " ");
            Console.WriteLine();
            z = -1;
            Console.Write("Введи столбец m= ");
            while (z < 0 || z > a.GetLength(1) - 1)
            {
                Console.WriteLine("Введите номер столбец от" + " 1" + " до " + (a.GetLength(1)));
                z = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine();
            for (int i = 0; i < a.GetLength(0) - 1; i++) Console.WriteLine(a[i, z - 1] + " ");
        }


        static void zamena(int[,] a)
        {
            int z = 0;
            for (int i = 0; i < a.GetLength(0) / 2; i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    z = a[i, j];
                    a[i, j] = a[a.GetLength(0) - i - 2, j];
                    a[a.GetLength(0) - i - 2, j] = z;
                }
            }
        }
        static void Main()
        {
            
            

            Random rand = new Random();
            //task1
            int[,] tr = new int[rand.Next(5, 10), rand.Next(5, 10)];
            Console.WriteLine("Массив=");
            Randoms(tr);
            vivod(tr);
            Console.WriteLine();
            vivodStroki(tr);
            Console.Write("Сумма эллементов 3-ей строки =");
            vivod3(tr);
            Console.WriteLine();
            //task2
            int[,] MASS2 = new int[rand.Next(5, 10), rand.Next(5, 10)];
            Randoms(MASS2);
            Console.WriteLine("Массив=");
            vivod(MASS2);
            Console.WriteLine("Смена строк местами -");
            zamena(MASS2);
            vivod(MASS2);
            Console.WriteLine();
            //task 4
            int[,] poezda = new int[18, 36];
            Randoms2(poezda);
            vivod(poezda);
            Mesta(poezda);
            Console.WriteLine();
            //task 5
            int z = rand.Next(5, 10);
            int[,] kvadrat = new int[z, z];
            random_Kvadrat(kvadrat);
            Console.WriteLine("Массив = ");
            vivod(kvadrat);
            Sum_elem(kvadrat);
            Console.WriteLine();
            POB_DIAG(kvadrat);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            //task6
            int[,] mas1 = new int[rand.Next(3, 10), rand.Next(3, 10)];
            Randoms(mas1);
            vivod(mas1);
            Console.WriteLine();
            onemas_vivod(ONEMASS(mas1));
            Console.WriteLine();
            Console.WriteLine();
            onemas_vivod(ONEMASS_str(mas1));
            Console.WriteLine();
            Console.WriteLine();
            //task7
            int[,] M7 = new int[7, 7];
            mas7(M7);
            vivod(M7);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            //task 8

            int[,] m5 = new int[5, 5];
            NULL(m5);
            M5(m5);
            vivod10(m5);

            
        }
    }
}


