﻿using System;

namespace lab6
{
    class Program
    {
        
        static void vivod(int[] mas)
         {
            for (int i=0; i < mas.Length; i++) 
            Console.Write(mas[i]+" ");
            Console.WriteLine();
         }


        static void VES(int[ ] ves)
        {
            Random rand = new Random();
            for (int i = 0; i < ves.Length; i++) ves[i] = rand.Next(50, 100);
        }

        static void Mas_RAND(int[]  Rand)
        {
            Random rand = new Random();
            for (int i = 0; i < Rand.Length; i++) Rand[i] = rand.Next(50, 100);
        }

        static void Mas_x2(int[] Rand)
        {
            for (int i = 0; i < Rand.Length; i++) Rand[i] =Rand[i]*2;
            vivod(Rand);
        }

        static void Mas_x1(int[] Rand)
        {

            int z = Rand[1];
            for (int i = 0; i < Rand.Length; i++) Rand[i] = Rand[i] /z;
            vivod(Rand);
        }

        static void Mas_x3(int[] Rand)
        {
            Random rand = new Random();
            int a = rand.Next(0, 100);
            for (int i = 0; i < Rand.Length; i++) Rand[i] = Rand[i]-a;
            vivod(Rand);
        }
       
        
        static void Random_mass(int[] vs)
        {
            Random rand = new Random();
            int i=0;
            while(i<vs.Length)
            {
                vs[i] = rand.Next(-500,500);
                if (vs[i] == 0) i--;
                i++;
            }
         }


        static string Mass_chotn(int[] rand)
        {
            int sum=0;
            
            for(int i=0;i<rand.Length;i++)
                sum = sum + rand[i];
            if (sum % 2 == 0) return "YES";
            else return "NO";
        }


        static string Mass_5(int[] rand)
        {
            int sum = 0;
            for (int i = 0; i < rand.Length; i++)
                sum = sum + (int)Math.Pow(rand[i],2);
            if (Convert.ToString(sum).Length== 5) return "YES";
            else return "NO";
        }


        static void Doma(int[] rand)
        {
            int sum1 = 0;
            int sum0 = 0;
            for (int i = 0; i < rand.Length; i =i +2)
                sum0 = sum0 + rand[i];
            for (int i = 1; i < rand.Length; i =i+2)
                sum1 = sum1 + rand[i];
            if (sum1 > sum0)
                Console.WriteLine("НА нечетной");
            else
         if (sum0 != sum1) Console.WriteLine("на четной");
          else  Console.WriteLine("по-ровну");
        }


        static int METOD1(int[] rand)
        {
            int i = 0;
            int koll = 0;
            while (i<rand.Length-1)
            { 
                if ((rand[i] > 0) && (rand[i + 1] < 0) || (rand[i] < 0) && (rand[i + 1] > 0))
                    koll++;
                i++;
             }
            return koll;
      }
        

        static void Main(string[] args)
        {
            //task 1 
            Console.WriteLine("Задание 1");
            Random rand = new Random();
            int[ ] Ves = new int[20];
            VES(Ves);
            vivod(Ves);
            //task 2
            Console.WriteLine("Задание 2");
            int[] mass = new int[rand.Next(0, 100)];
            Console.WriteLine("До");
            Mas_RAND(mass);
            vivod(mass);
            Console.WriteLine("2.а");
            Mas_x2(mass);
            
            Console.WriteLine("2.2б");
            Mas_x1(mass);
            
            Console.WriteLine("2.3в");
                  Mas_x3(mass);
                       
            //task3
            int[] Massi = new int[rand.Next(0, 100)];
            Console.WriteLine();
            Mas_RAND(Massi);
            Console.WriteLine("Задание 3");
            vivod(Massi);
            Console.WriteLine( Mass_chotn(Massi));
            Console.WriteLine(Mass_5(Massi));
            //task 4
            Console.WriteLine("Задание 4");
            int[] Dom = new int[rand.Next(0, 100)];
            Mas_RAND(Dom);
            vivod(Dom);
           Doma(Dom);
            //task 5
            Console.WriteLine("Задание 5");
            int[] NULL = new int[rand.Next(0, 100)];
            Random_mass(NULL);
            vivod(NULL);
           Console.WriteLine( METOD1(NULL)+" раза");
        }
    }
}
