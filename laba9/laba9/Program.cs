﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace laba9
{
    class Program
    {
        static void slov(ref StringBuilder PER)
        {
                while ((PER.Length >0)&&(!(Char.IsLetter(PER[PER.Length - 1]))))
                {
                    PER = PER.Remove(PER.Length - 1, 1);
                }
                while ((PER.Length >0) &&( !(Char.IsLetter(PER[0]))))
                {
                    PER = PER.Remove(0, 1);
                }
            }
        static void Main()
        {
            Console.Write("Введите строку ");
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            string stroka = Console.ReadLine();
            StringBuilder PER;

            Console.WriteLine(); Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" Задание 1");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Введите максимальную длинну слова   ");
            int N = Convert.ToInt32(Console.ReadLine());
            string[] slova;
            slova = stroka.Split(' ');
          
         
            for (int i = 0; i < slova.Length; i++)

            
                {
                PER = new StringBuilder(slova[i]);
                slov(ref PER);
                if (PER.Length <= N)
                    Console.Write(PER + " ");
            }
                Console.WriteLine(); Console.WriteLine(); Console.WriteLine();


                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(" Задание 2");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Слова начинающиеся с прописной буквы   ");
                for (int i = 0; i < slova.Length; i++)
                {
                    PER = new StringBuilder(slova[i]);
                slov(ref PER);
                
                if ((PER.Length!=0)&&(char.IsLetter(PER[0]))&& (!(char.IsUpper(PER[0]))))
                        Console.Write(PER + " ");
                }
            Console.WriteLine(); Console.WriteLine(); Console.WriteLine();


            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" Задание 3");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Слова по 1 разу ");
            string[] slova_t = new string[slova.Length];
            PER = new StringBuilder(slova[0]);
            slov(ref PER);
            int j;
            Console.Write(PER + " ");
            for (int i = 0; i < slova.Length; i++)
            {
                PER = new StringBuilder(slova[i]);
                slov(ref PER);
                slova_t[i] = Convert.ToString(PER);
            }
            for (int i = 0; i < slova.Length; i++)
            {
                j = 0;
                while( (j < i)&& (slova_t[i] != slova_t[j]))
                        j++;
                if (j == i)
                {
                    j++;
                    while ((j  < slova_t.Length-1) && (slova_t[i] != slova_t[j]))
                        j++;
                }
                if (j == slova_t.Length-1)
                    Console.Write(PER + " ");
            }
            Console.WriteLine(); Console.WriteLine(); Console.WriteLine();


            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" Задание 4");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Введите слово для проверки ");
            string str = Console.ReadLine();
            int k = 0;
            for (int i = 0; i < slova.Length; i++)
                if (str == slova_t[i])
                    k++;
            Console.WriteLine("Данное слово встречается  " + k + " раз");
            Console.WriteLine(); Console.WriteLine();


            Console.ForegroundColor = ConsoleColor.White; 
            Console.WriteLine(" Задани 5");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            str = Convert.ToString(slova_t[0]);
            for (int i = 1; i < slova_t.Length - 1; i++) 
                if (str.Length < slova_t[i].Length)
                    str = slova_t[i];
            Console.WriteLine("Самое длинное слово " + str);
            Console.WriteLine(); Console.WriteLine();


            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" Задание 6 ");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Слова которые встречаются ровно 1 раз ");
            for (int i = 0; i < slova_t.Length; i++)
            {
                j = 0;
                while ((j < i) && (slova_t[i] != slova_t[j]))
                    j++;

                if (j == i)
                    j++;
                while ((j < slova_t.Length - 1) && (slova_t[i] != slova_t[j]))
                    j++;
                if (j == slova_t.Length - 1)
                    Console.Write(slova_t[i] + " ");
            }
                Console.WriteLine(); Console.WriteLine();


                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(" Задание 7");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Введиете число  N");
            N = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < slova_t.Length; i++)
            {
                j = 0;
                k = 0;
                while (j < i)
                {
                    if (slova_t[j] != slova_t[i])
                        j++;
                    else break;
                }
                if (j == i)
                {
                    j = 0;
                    while ((j < slova_t.Length) && (k <= N))
                    {
                        if (slova_t[i] == slova_t[j])
                            k++;
                        j++;
                    }
                }
                if (k > N)
                    Console.Write(slova_t[i] + " ");
            }
                    Console.WriteLine(); Console.WriteLine();


                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(" Задание 8");
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Слова по алфавитному порядку");
                    List<string> Words = slova_t.ToList();
                    Words.Sort();
                    Console.WriteLine(string.Join( " ", Words));
            Console.WriteLine(); Console.WriteLine();


            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" Задание 9");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Слова по возрастанию  порядку");
            Words =Words.OrderBy(x => x.Length).ToList();
            Console.WriteLine(string.Join(" ", Words));

        }
    }
}
