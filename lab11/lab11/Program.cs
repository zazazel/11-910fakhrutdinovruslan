﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;


namespace lab11
{
    class Program
    {
        static void Reg(ref Regex regex, string pattern)
        {
            regex = new Regex(pattern);
        }
        static void Reg_Vivod(Regex regex, string str)
        {
            foreach (Match match in regex.Matches(str))
                Console.WriteLine(match.Value);
        }
        static void TASK5(ref Regex regex, string str)
        {
            string [] a = new string[3];
            foreach (Match match in regex.Matches(str))
            {
              a= match.Value.Split('.');
                if (Convert.ToInt32(a[0]) == 31)
                    if (Convert.ToInt32(a[1]) != 12)
                        Console.WriteLine("1." + (Convert.ToInt32(a[1]) + 1) + "." + a[2]);
                    else
                        Console.WriteLine("1." + "1" + "." + (Convert.ToInt32(a[2]) + 1));
                else
                    Console.WriteLine((Convert.ToInt32(a[0]) + 1) + "." + a[1] + "." + a[2]);
            }
         }
        static void Main()
        {
            string[] mask;
            MatchCollection matches;
            Regex regex = new Regex("");
            string pattern;
            string str = "12-32-44asfm afkafl 21-34-32-da a-122-23-44 11.5.2000 11.11.2000 31.12.2000  31.5.2000 9.12.2000 62.16.64.0 - 62.16.95.25562.61.0.0 - 62.61.31.25562.64.0.0 - 62.64.31.25562.69.0.0 - 62.69.31.255Союз «Архангельская ТПП» http://arkhangelsk.tpprf.ru Союз «Вологодская ТПП» http://vologda.tpprf.ru/ Союз «ТПП г. Череповца» http://www.tpp-cherepovets.ru/ Союз «Калининградская ТПП» http://www.kaliningrad-cci.ru/ Союз «Новгородская ТПП»";
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine(" Лабараторная работа №11");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("_________________________");
            Console.WriteLine("");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("");
            Console.WriteLine("Задание 1");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");
            pattern = @"\d{2}-\d{2}-\d{2}|\d{3}-\d{2}-\d{2}|\d{4}-\d{2}";
            Reg(ref regex, pattern);
            Console.WriteLine(" В данном сообщение встречается " + regex.Matches(str).Count.ToString() + " номеров");
            Reg_Vivod(regex, str);

            Console.WriteLine("");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("");
            Console.WriteLine("Задание 2");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");


            pattern = @"([1-9]{1}|[10-31]{2})\.([1-9]{1}|[10-12]{2})\.[1900-2010]{4}";
            Reg(ref regex, pattern);
            Console.WriteLine(regex);
            matches = regex.Matches(str);
            Console.WriteLine(" В тексте встретилось " + matches.Count.ToString() + " дат");
            Reg_Vivod(regex, str);
            Console.WriteLine("");

            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("");
            Console.WriteLine("Задание 3");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");

            Console.Write(" Введите число для удаления: ");
            pattern = @"([0-9]{1}|[10-99]{2}|[100-255]{3})\.([0-9]{1}|[10-99]{2}|[100-255]{3})\.([0-9]{1}|[10-99]{2}|[100-255]{3})\." + Console.ReadLine();
            Reg(ref regex, pattern);
            Console.WriteLine(" В тексте встретилось " + regex.Matches(str).Count.ToString() + " IP-адресов с данным числом");
            Reg_Vivod(regex, str);
            Console.WriteLine("");

            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("");
            Console.WriteLine("Задание 4");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");

            pattern = @"((https?|ftp)\:\/\/)?([a-z0-9]{1})((\.[a-z0-9-])|([a-z0-9-]))*\.([a-z]{2,6})(\/?)";
            Reg(ref regex, pattern);
            Console.WriteLine(" Все адресса web-сайтов встретившихся в тексте");
            Reg_Vivod(regex, str);
            Console.WriteLine("");

            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("");
            Console.WriteLine("Задание 5");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");

            pattern = @"([1-9]{1}|[10-31]{2})\.([1-9]{1}|[10-12]{2})\.[1900-2010]{4}";
            Reg(ref regex, pattern);
            matches = regex.Matches(str);
            Console.WriteLine(" В тексте встретилось " + matches.Count.ToString() + " дат");
            TASK5(ref regex, str);

            Console.WriteLine("");
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine(" THE END");
            Console.ForegroundColor = ConsoleColor.DarkGray;
         }
     }
 }
