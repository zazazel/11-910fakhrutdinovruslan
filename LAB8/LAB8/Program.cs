﻿using System;
using System.Text;
using System.Linq;

namespace LAB8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("TASK1");
            Console.Write("Введите строку     ");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            String stroka = Console.ReadLine();
            StringBuilder str = new StringBuilder(stroka);
            var Buff = str[0];
            for (int i = 0; i < str.Length - 1; i += 2)
            {
                Buff = str[i];
                str[i] = str[i + 1];
                str[i + 1] = Buff;
            }
            Console.WriteLine();
            Console.WriteLine("Строка после изменения = " + str);
            Console.WriteLine(); Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("TASK2");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(" Символов в ведённой строке = " + stroka.Length);
            Console.WriteLine(); Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("TASK 3");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            str = new StringBuilder(stroka);
            bool flag = false;
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str[i] == str[i + 1])
                {
                    flag = true;
                    break;
                }
            }
            if (flag)
                Console.WriteLine("Есть");
            else
                Console.WriteLine("Нет");
            Console.WriteLine(); Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("TASK 4");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            str = new StringBuilder(stroka);
            if ((stroka.Length) % 2 == 0)
                str.Remove((str.Length / 2) - 1, 2);
            else
                str.Remove((stroka.Length / 2), 1);
            Console.WriteLine("После удаления среднего символа строка  имеет вид " + str);
            Console.WriteLine(); Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("TASK 5");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.Write("Введите подстроку 1 которую хотите заменить   ");
           string Buff1=Console.ReadLine();
            Console.Write("Введите подстроку 2 на которую хотите заменить   ");
            string Buff2 = Console.ReadLine();
            str = new StringBuilder(stroka);
            str = str.Replace(Buff1, Buff2);

            Console.WriteLine("Строка после изменения " + str);
            Console.WriteLine(); Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("TASK 6");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            int sum = 0;
            for (int i = 0; i < stroka.Length; i++)
                if (Char.IsNumber(stroka[i]))
                    sum = sum + Convert.ToInt32(Convert.ToString(stroka[i]));
            Console.WriteLine("Сумма всех чисел строки = " + sum);
            Console.WriteLine(); Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("TASK 7");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.Write("Символлы до первого ':'    ");
            for (int i = 0; i < stroka.Length; i++)
                if (stroka[i] != ':')
                    Console.Write(stroka[i] + " ");
                else
                    break;
            Console.WriteLine(); Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("TASK 8");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            int z = 0;
            for (int i = stroka.Length - 1; i > -1; i--)
                if (stroka[i] != ':')
                    z++;
                else
                    break;
            Console.WriteLine();
            Console.Write("Символлы после последнего   ':'    ");
                for (int i = stroka.Length - z; i < stroka.Length; i++)
                    if (stroka[i] != ':')
                        Console.Write(stroka[i] + " ");
            Console.WriteLine(); Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("TASK 9");
            Console.ForegroundColor = ConsoleColor.DarkGreen;

            str = new StringBuilder(stroka);
            str = str.Remove(stroka.IndexOf(',')+1, stroka.LastIndexOf(',') - stroka.IndexOf(',') - 1) ;
            Console.WriteLine("Строка после изменения "+str);
            Console.WriteLine(); Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("TASK 10");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            int t = stroka.Distinct().Count();
            Console.WriteLine("В строке разных символов = аа" + t);
        }
    }
}
