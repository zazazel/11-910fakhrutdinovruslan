﻿using System;
using System.Text.RegularExpressions;
using System.Text;
using System.Linq;


namespace lab10
{
    class Program
    {
        static void Main()
        {
            string text = "Речь идет о встречных исковых требованиях украинской стороны в рамках спора с российской компанией, касающегося контракта на транзит газа. Сумма исковых требований «Нафтогаза» составляет порядка $12 млрд" +
"Украинская компания «Нафтогаз» подала новый иск в Стокгольмский арбитраж против российской компании «Газпром», касающийся контракта на транзит газа.Об этом сообщил исполнительный директор украинской компании Юрий Витренко на своей странице в Facebook." +
"Речь идет о встречных исковых требованиях, которые украинская сторона предъявляет российской компании. «Наступила наша очередь подать «Заявление о защите и встречные исковые требования». «Количество страниц только основного текста нашего заявления превышает 500 страниц.К нему прилагаются три отчета международных экспертов, которые были специально для этого разработаны. Также прилагаются показания свидетелей и куча других приложений», — написал Витренко." +
"Сумма иска «Нафтогаза» может составить порядка $12 млрд." +
"РБК направил запрос в пресс - службу «Газпрома» по поводу нового иска «Нафтогаза»";

            Console.WriteLine(text);
            Console.WriteLine("Task 1");
            Console.WriteLine("Какое слово вы ищите ?");
            string pattern = Console.ReadLine();
            Regex r = new Regex(@"\b" + pattern + @"\b", RegexOptions.IgnoreCase);
            if (r.IsMatch(text))
            {
                Console.WriteLine("в строке есть слово " + pattern);
            }
            else
                Console.WriteLine("в строке нет слова " + pattern);

            Console.WriteLine();
            Console.WriteLine("Task 2");
            Console.WriteLine("Введите длинну строки ");

            int pattern1 = Convert.ToInt32(Console.ReadLine());
            r = new Regex(@"\b\w{" + pattern1 + @"}\b");
            MatchCollection matches = r.Matches(text);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    Console.WriteLine(match.Value);
            }
            else
            {
                Console.WriteLine("Совпадений не найдено");
            }

            Console.WriteLine();
            Console.WriteLine("Task 3");
            Console.WriteLine();

            r = new Regex(@"\b[А-Я]\w*\b");
            matches = r.Matches(text);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    Console.WriteLine(match.Value);
            }
            else
            {
                Console.WriteLine("Совпадений не найдено");
            }

            Console.WriteLine();
            Console.WriteLine("Task 4");
            Console.WriteLine();

            string target= " ";
            r = new Regex(@"\W");
            string result = r.Replace(text, target);
            Console.WriteLine(result);

            Console.WriteLine();
            Console.WriteLine("Task 5");
            Console.WriteLine();

            target= "...";
            r = new Regex(@"\b\w[a-z]+\b", RegexOptions.IgnoreCase);
            result = r.Replace(text, target);
            Console.WriteLine(result);
           
            Console.WriteLine();
            Console.WriteLine("Task 6");
            Console.WriteLine();

            double sum = Regex.Matches(text, @"\d*\.?\d+").Cast<Match>().Select(m => m.Value).Select(double.Parse).Sum();
            Console.WriteLine(sum);



        }
    }
}
