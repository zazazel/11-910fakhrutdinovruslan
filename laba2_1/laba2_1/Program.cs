﻿using System;

namespace laba2_1
{
    class Program
    {
        static void Main()
        {
            #region main
            Console.WriteLine("1) Задачи на графики");
            Console.WriteLine("2) Определение масти");
            Console.WriteLine("3) Определение лостоинства карты");
            Console.WriteLine("4)Опроделение масти и достоинства");
            Console.WriteLine("5) Максимальная скорость");
            Console.WriteLine("6)Задача 7");
            int n = Convert.ToInt32(Console.ReadLine());
            switch (n)
            {
                case 1:
                    {
                        Console.WriteLine("Введите координаты для 1");
                        task_1();
                        Console.WriteLine("Введите координаты для 2");
                        task1_1();
                        Console.WriteLine("Введите координаты для 3");
                        task1_2();
                        Console.WriteLine("Введите координаты для 4");
                        task1_3();
                        Console.WriteLine("Введите координаты для 5");
                        task1_4();
                        Console.WriteLine("Введите координаты для 6");
                        task1_5();
                        break;
                    }
                case 2:
                    {
                        task_2();
                        break;
                    }
                case 3:
                    {
                        task3();
                        break;
                    }
                case 4:
                    {
                        Task4();
                        break;
                    }
                case 5:
                    {
                        task5();
                        break;
                    }
            }
            #endregion
        }
        #region grafics
        static void task_1()
        {
            switch (check())
            {
                case 0:
                    {
                        Console.WriteLine("да");
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("нет");
                        break;
                    }
                case 1:
                    {
                        Console.WriteLine("на границе");
                        break;
                    }
            }
        }
        public static int check()
        {
            int z = 2;
            double X = Convert.ToInt32(Console.ReadLine());
            double Y = Convert.ToInt32(Console.ReadLine());
            if ((Y * Y + X * X < 9 * 9) && (X > 0)) z = 0;
            if ((Y * Y + X * X == 9 * 9) && (X >= 0)) z = 1;
            if ((Y * Y < 81) && (X == 0)) z = 1;
            return z;
        }
        static void task1_1()
        {
            switch (check2())
            {
                case 0:
                    {
                        Console.WriteLine("да");
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("нет");
                        break;
                    }
                case 1:
                    {
                        Console.WriteLine("на границе");
                        break;
                    }
            }
            Main();
        }
        public static int check2()
        {
            int z = 2;
            double X = Convert.ToInt32(Console.ReadLine());
            double Y = Convert.ToInt32(Console.ReadLine());
            if ((X * X + Y * Y < 8 * 8) && (X * X + Y * Y > 3 * 3) && (X < 0)) z = 0;
            if (((X * X + Y * Y) == (8 * 8)) && (X < 0)) z = 1;
            if ((3 < (Math.Abs(Y)) && (Math.Abs(Y) < 8)) && (X == 0)) z = 1;
            if ((X * X + Y * Y == 3 * 3) && (X < 0)) z = 1;
            return z;
        }
        static void task1_2()
        {
            switch (check_3())
            {
                case 0:
                    {
                        Console.WriteLine("да");
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("нет");
                        break;
                    }
                case 1:
                    {
                        Console.WriteLine("на границе");
                        break;
                    }
            }
            Main();
        }
        public static int check_3()
        {
            int z = 2;
            double X = Convert.ToInt32(Console.ReadLine());
            double Y = Convert.ToInt32(Console.ReadLine());
            if (X * X + Y * Y < 15 * 15) z = 0;
            if (X * X + Y * Y > 25 * 25) z = 0;
            if (X * X + Y * Y == 15 * 15) z = 1;
            if (X * X + Y * Y == 25 * 25) z = 1;
            return z;
        }
        public static void task1_3()
        {
            Console.WriteLine("Введите координаты точки ");
            switch (check_4())
            {
                case 0:
                    {
                        Console.WriteLine("да");
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("нет");
                        break;
                    }
                case 1:
                    {
                        Console.WriteLine("на границе");
                        break;
                    }
            }
            Main();
        }
        public static int check_4()
        {
            int z = 2;
            int x, y;
            x=Convert.ToInt32(Console.ReadLine());
            y = Convert.ToInt32(Console.ReadLine());
            if ((x * x + y * y < 15 * 15) | | (x * x + y * y > 25 * 25)) z = 0;
            if ((x * x + y * y > 15 * 15) && (x * x + y * y < 25 * 25)) z = 2;
            if ((x * x + y * y == 15 * 15) | | (x * x + y * y == 25 * 25)) z = 1;
            return z;
        }
        public static void task1_4()
        {
            Console.WriteLine("Введите координаты для проверки");
            switch (check_5())
            {
                case 0:
                    {
                        Console.WriteLine("да");
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("нет");
                        break;
                    }
                case 1:
                    {
                        Console.WriteLine("на границе");
                        break;
                    }
            }
            Main();
        }
        public static int check_5()
        {
            Console.WriteLine("Введите координаты для проверки");
            int z = 2;
            int x, y;
            x = Convert.ToInt32(Console.ReadLine());
            y = Convert.ToInt32(Console.ReadLine());
            if ((y < 0) && (y > -100) && (y < -Math.Abs(x))) z = 0;
            if (((y == -Math.Abs(x)) | | ((y == -100) && (Math.Abs(x) <= 100))) && (y >= 100)) z = 1;
            return z;
        }
        public static void task1_5()
        {
            Console.WriteLine("Введите координаты для проверки");
            switch (check_6())
            {
                case 0:
                    {
                        Console.WriteLine("да");
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("нет");
                        break;
                    }
                case 1:
                    {
                        Console.WriteLine("на границе");
                        break;
                    }
            }
            Main();
        }
        public static int check_6()
        {
            int z = 2;
            int x, y;
            x = Convert.ToInt32(Console.ReadLine());
            y = Convert.ToInt32(Console.ReadLine());
            if ((Math.Abs(x) > 40) | | (Math.Abs(y) > 40)) z = 0;
            if ((Math.Abs(x) == 40) & (Math.Abs(y) <= 40)) z = 1;
            if ((Math.Abs(y) == 40) & (Math.Abs(x) <= 40)) z = 1;
            return z;
         }
        #endregion
        public static void Task4()
        {
            Console.WriteLine();
            Tuple<int, int> card = new Tuple<int, int>(Convert.ToInt32(Console.ReadLine()), Convert.ToInt32(Console.ReadLine()));
            switch (card.Item1)
            {
                case 1:
                    {
                        Console.Write("Трефы");
                        break;
                    }
                case 2:
                    {
                        Console.Write("Бубны");
                        break;
                    }
                case 3:
                    {
                        Console.Write("Червы");
                        break;
                    }
                case 4:
                    {
                        Console.Write("Пики");
                        break;
                    }

            }
            Console.Write(" ");
            switch (card.Item2)
            {
                case (6):
                    {
                        Console.WriteLine("шесть");
                        break;
                    }
                case (7):
                    {
                        Console.WriteLine("Семь");
                        break;
                    }
                case (8):
                    {
                        Console.WriteLine("Восемь");
                        break;
                    }
                case (9):
                    {
                        Console.WriteLine("Девять");
                        break;
                    }
                case (10):
                    {
                        Console.WriteLine("Десять");
                        break;
                    }
                case (11):
                    {
                        Console.WriteLine("Валет");
                        break;
                    }
                case (12):
                    {
                        Console.WriteLine("Дама");
                        break;
                    }
                case (13):
                    {
                        Console.WriteLine("Король");
                        break;
                    }
                case (14):
                    {
                        Console.WriteLine("Туз");
                        break;
                    }
            }
            Main();
        }
        public static void task3()
        {
            Console.WriteLine("Введите достоинство карты от 6 до 14 ");
            switch(Convert.ToInt32(Console.ReadLine()))
            {
                case 6:
                {
                        Console.WriteLine("Шестерка");
                    break;
                }
                case 7:
                    {
                        Console.WriteLine("Семь");
                        break;
                    }
                case 8:
                    {
                        Console.WriteLine("Восьмерка");
                        break;
                    }
                case 9:
                    {
                        Console.WriteLine("Девятка");
                        break;
                    }
                case 10:
                    {
                        Console.WriteLine("Десять");
                        break;
                    }
                case 11:
                    {
                        Console.WriteLine("Валет");
                        break;
                    }
                case 12:
                    {
                        Console.WriteLine("Дама");
                        break;
                    }
                case 13:
                    {
                        Console.WriteLine("Король");
                        break;
                    }
                case 14:
                    {
                        Console.WriteLine("Туз");
                        break;
                    }
                    Console.WriteLine();
            }
        }
        public static void task_2()
        {
            string Mast = "";
            Console.WriteLine("Введите номер масти ");
            Console.WriteLine("1-Пики 2-Трефы 3-Бубны 4-Черви");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    {
                        Mast = "Пики";
                        break;
                    }
                case 2:
                    {
                        Mast = "Трефы";
                        break;
                    }
                case 3:
                    {
                        Mast = "Бубны";
                        break;
                    }
                case 4:
                    {
                        Mast = "Черви";
                        break;
                    }
            }
            Console.WriteLine(Mast);

        }
        public static void task5()
        {
            Console.WriteLine("Введите признак  транспортного средства ");
            Console.WriteLine("А-автомобиль, в-велосипед, м-мотоцикл, с-самолёт, п-поезд ");
            string ts;
            ts = Convert.ToString(Console.ReadLine());
            switch(ts)
            {
                case ("A" | | "a" | | "а" | | "А"):
                    {
                        Console.WriteLine("Максимальная скорость автомобиля 464 км/ч");
                        break;
                    }
                case ("b" | | "B" | | "в"  |  |"В"):
                    {
                        Console.WriteLine("Максимальная скорость велосипеда -263  км/ч");
                        break;
                    }
                case ("m" | | "M" || "м" | | "М") :
                    {
                        Console.WriteLine("Максимальная скорость мотоцикла - 408 км/ч");
                        break;
                    }
                case ("c" | | "C" | |  "с" | |  "С")    :
                    {
                        Console.WriteLine("Максимальная скоростьсамолёта 8200 км/ч");
                        break;
                    }
                case  ("p" | | "P" | | "П" | | "п"):
                    {
                        Console.WriteLine("Максимальная скорость 581 км/ч");
                        break;
                    }

            }
        }
    }
}





